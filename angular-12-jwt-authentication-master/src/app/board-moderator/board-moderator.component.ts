import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../_services/Employee';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css'],
})
export class BoardModeratorComponent implements OnInit {
  employee: Employee[] = [];
  content?: string;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.userService.getAdminBoard().subscribe(
      (data) => {
        this.employee = data;
      },
      (err) => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }

  deleteById(id: number) {
    this.router.navigate(['/update', id]);
  }
}
