import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from './Employee';

const API_URL = 'http://localhost:8080/api/test/';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }

  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }

  // getAdminBoard(): Observable<any> {
  //   return this.http.get(API_URL + 'admin', { responseType: 'text' });
  // }

  //getEmployeeAllList
  getAdminBoard(): Observable<Employee[]> {
    return this.http.get<Employee[]>(API_URL + 'admin');
  }

  getEmployeeById(id: number): Observable<Employee> {
    return this.http.get<Employee>(API_URL + 'admin/' + id);
  }

  deleteById(id: number): Observable<Object> {
    return this.http.delete(API_URL + 'admin/' + id);
  }

  updateEmployee(id: number, employee: Employee): Observable<Object> {
    return this.http.put(API_URL + 'admin/' + id, employee);
  }
}
